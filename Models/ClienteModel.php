<?php
require_once ('../Db/DBManager.php');


function getImage(){

    $manager = new DBManager();
    try{
        $sql = 'SELECT imagen FROM cliente WHERE id=3';
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result[0]['imagen'];
    }catch (PDOException $e){
        echo $e->getMessage();
    }
}

function updateCliente($image){

    $manager = new DBManager();
    try {
        $sql = 'UPDATE cliente SET imagen=:img WHERE id=3';
        $stmt = $manager->getConexion()->prepare($sql);
        $stmt->bindValue(':img',$image,PDO::PARAM_LOB);
        $stmt->execute();

    }catch(PDOException $e){
        echo $e->getMessage();
    }

}
function insertCliente($cliente){
    $manager = new DBManager();
    try{
        $sql="INSERT INTO cliente (nombre, apellidos, fecha_nacimiento, sexo, telefono, dni, email, password) VALUES (:nombre, :apellidos, :fecha_nacimiento, :sexo, :telefono, :dni, :email, :password)";
        $nombre=$cliente->getName();

        $apellidos=$cliente->getSurname();
        $fecha_nacimiento=$cliente->getBirthdate();
        $sexo=$cliente->getGenre();
        $telefono=$cliente->getMobilePhone();
        $dni=$cliente->getDni();
        $email=$cliente->getEmail();
        $password=$cliente->getPass();

        $stmt = $manager->getConnection()->prepare($sql);
        $stmt->bindParam(':nombre',$nombre);
        $stmt->bindParam(':apellidos',$apellidos);
        $stmt->bindParam(':fecha_nacimiento',$fecha_nacimiento);
        $stmt->bindParam(':sexo',$sexo);
        $stmt->bindParam(':telefono',$telefono);
        $stmt->bindParam(':dni',$dni);
        $stmt->bindParam(':email',$email);
        $stmt->bindParam(':password',$password);

        if($stmt->execute()){
            echo "todo OK";
        }else{
            echo "MAL";
        }
    }catch(PDOException $e){
        echo $e->getMessage();
    }
}

function getUserHash($dni){
    $conexion = new DBManager();
    try{
        $sql = "SELECT * FROM cliente WHERE dni=:dni";
        $stmt = $conexion->getConnection()->prepare($sql);
        $stmt->bindParam(':dni',$dni);
        $stmt->execute();
        error_log('------------------------>' . $dni);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $result[0]['password'];
    }catch (PDOException $e){
        echo $e->getMessage();
    }


}