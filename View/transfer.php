<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <title>Transfer</title>
</head>

<body>
    <h1 class="text-secondary border text-center">Transfer</h1>
    <nav class="nav">
        <a class="nav-link" href="profile.php">Profile</a>
        <a class="nav-link" href="init.php">Init</a>
        <a class="nav-link active" href="transfer.php">Transfer</a>
        <a class="nav-link" href="logout.php">Logout</a>
    </nav>
    <form action="../Controller/controller.php" method="post">
        <div class="form-group col">
            <label for="accountNumber">IBAN:</label>
            <input name="accountNumber" type="text" class="form-control" >
        </div>
        <div class="form-group col">
            <label for="amountOfMoney">Cantidad:</label>
            <input name="amountOfMoney" type="text" class="form-control" >
        </div>
        <div class="form-group col">
            <label for="annotation">Anotación:</label>
            <textarea name="annotation" id="annotationTransfer" cols="50" rows="2" ></textarea>
        </div>
        <div class="form-group col">
            <input type="hidden" class="form-control" value="transfer" name="control">
        </div>
        <div class="form-group col">
            <input type="submit" class="form-control btn btn-primary" name="submit" value="submit">
        </div>
    </form>
    <form action="controller.php" method="post">
        <select name="cuentas">

            <?php
            require_once('model/CuentaModel.php');
            $accounts=getAccounts('dni');
            for ($i=0; $i<sizeof($accounts) ;$i++){?>
                <option ><?php echo $accounts[$i]["cuenta"] ?></option>
            <?php }?>
        </select>
        Cuenta destino: <input name="cuenta_destino" type="text" />
        Cantidad: <input name="cantidad" type="text" />
        <input name="submit" type="submit" value="Seleccionar"/>
        <input name="control" type="hidden" value="transfer"/>
    </form>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

</body>

</html>

<?php
