<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


  <title>Profile</title>
</head>

<body>
  <h1 class="text-secondary border text-center">Profile</h1>
  <nav class="nav">
    <a class="nav-link active" href="profile.php">Profile</a>
    <a class="nav-link" href="init.php">Init</a>
    <a class="nav-link" href="transfer.php">Transfer</a>
    <a class="nav-link" href="logout.php">Logout</a>
  </nav>
  <form action="../Controller/controller.php" method="post">
    <div class="form-group col">
      <label for="userName">Nombre:</label>
      <input name="userName" type="text" class="form-control" >
    </div>
    <div class="form-group col">
      <label for="userSurname">Apellidos:</label>
      <input name="userSurname" type="text" class="form-control">
    </div>
    <div class="form-group col">
      <label for="genre">Selecciona tu género:</label>
      <select class="form-control">
        <option>Hombre</option>
        <option>Mujer</option>
      </select>
    </div>
    <div class="form-group col">
      <label for="birthdate">Fecha de nacimiento:</label>
      <input name="birthdate" type="date" class="form-control">
    </div>
    <div class="form-group col">
      <label for="mobileNumber">Nº teléfono:</label>
      <input name="mobilePhone" type="tel" class="form-control" >
    </div>
    <div class="form-group col">
      <label for="email">Email:</label>
      <input name="email" type="email" class="form-control" >
    </div>
    <div class="form-group col">
      <label for="pass">Contraseña:</label>
      <input name="pass" type="password" class="form-control" >
    </div>
    <div class="form-group col">
      <label for="repeatPass">Contraseña:</label>
      <input name="repeatPass" type="password" class="form-control" >
    </div>
      <div>
          <input type="file" name="upload" id="upload"><br/>
      </div>
    <div class="form-group col">
      <input type="hidden" class="form-control" value="profile" name="control">
    </div>
    <div class="form-group col">
      <input type="submit" class="form-control btn btn-primary" name="submit" value="submit">
    </div>
  </form>

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>

</html>