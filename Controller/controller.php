<?php

require_once('../Models/Client.php');
require_once('../Models/ClienteModel.php');
require_once('../Models/Cuenta.php');
require_once('../Models/CuentaModel.php');
require_once('../Models/MovimientoModel.php');
require_once('../Helpers/helper.php');

if (isset($_POST['submit'])) {
    if ($_POST['control'] == 'register') {

        $cliente = new Client($_POST['nombre'], 'fecha_nacimiento', 'apellidos', 'sexo', 'email', 'telefono', 'dni', password_hash($_POST['pass']), '');
        insertCliente($cliente);

        $_POST['message'] = validateRegister();
        if ($_POST['message'] == 'OK') {
            header('Location: ../View/login.php');
        } else {
            require_once('../View/register.php');
        }
    } elseif ($_POST['control'] == 'login') {
        //$_POST['message'] = validateRegister();

        $hash = getUserHash($_POST['dni']);
        if (password_verify($_POST['pass'], $hash)) {
            header('Location: ../View/init.php');
        } else {
            require_once('../View/login.php');
        }
    }


    if ($_POST['control'] == 'profile') {
        $check = getimagesize($_FILES['upload']['tmp_name']);
        $fileName = $_FILES['upload']['name'];
        $fileName = $_FILES['upload']['size'];
        $fileName = $_FILES['upload']['type'];
        $image = file_get_contents($_FILES['upload']['tmp_name']);
        if ($check !== false) {
            updateCliente($image);
        }
    }

    if ($_POST['control'] == 'create') {
        session_start();
        createAccount($_SESSION['user']);
    }


    if ($_POST['control'] == 'select_account') {
        $saldo = getSaldo($_POST['cuentas']);
        session_start();
        $_SESSION['saldo'] = $saldo;
        header("Location: query.php");

    }


    if ($_POST['control'] == 'transfer') {

        if (existeCuenta($_POST['cuentas']) && existeCuenta($_POST['cuenta_destino'])) {
            transfer($_POST['cuentas'], $_POST['cuenta_destino'], $_POST['cantidad']);
        }
    }


    if ($_POST['control'] == 'query') {
        session_start();
        $_SESSION['saldo'] = getSaldo($_POST['cuentas']);
        $_SESSION['lista'] = getMovimientos($_POST['cuentas']);
        header("Location: query.php");
    }


    if ($_POST['control'] == 'select_account') {
        $saldo = getSaldo($_POST['cuentas']);
        session_start();
        $_SESSION['saldo'] = $saldo;
        $_SESSION['lista'] = getMovimientos($_POST['cuentas']);
        header("Location: query.php");

    }
}