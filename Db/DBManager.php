<?php
require_once('../Config/config.php');

class DBManager extends PDO
{
    public $server = SERVER;
    public $user = USER;
    public $pass = PASS;
    public $port = PORT;
    public $db = DB;

    private $connection;

    public function __construct(){
        $this -> conectar();
    }

    private final function conectar(){
        $connection = null;

        try {
            if (is_array(PDO::getAvailableDrivers())){
                error_log("Error DBMANAGER 1");
                print_r(PDO::getAvailableDrivers());
                if (in_array("pgsql", PDO::getAvailableDrivers())){
                    error_log("Error DBMANAGER 2"  );
                    $connection = new PDO("pgsql:host=$this->server;user=$this->user;password=$this->pass;port=$this->port;dbname=$this->db");

                }else{

                    throw new PDOException("Error DBMANAGER"  );
                }
            }
        }catch (PDOException $e){
            error_log("Error DBMANAGER" . $e->getMessage());
            echo $e->getMessage();
        }
        $this->setConnection($connection);
    }

    public final function setConnection($connection){
        $this->connection = $connection;
    }

    public final function getConnection(){
        return $this->connection;
    }

    public final function closeConnection(){
        $this->setConnection(null);
    }


}